![Mapa](http://www.digitalsurgeons.com/wp-content/uploads/2010/12/geolocation.png)

# Configuración

    Primero deben instalarse Node.js (http://nodejs.org/download/).

    Instalar mongodb (http://www.mongodb.org/downloads). Detalles de instalacion diferentes OS.

        Ubuntu -> (http://docs.mongodb.org/manual/tutorial/install-mongodb-on-ubuntu/)

    Instalar redis (http://redis.io/download)

        Ubuntu -> Detalles en la pagina de descarga.

    Instalar las dependencias del proyecto:

        Antes de iniciar por primera vez la aplicacion debes instalar las dependencias.

            npm install o (sudo npm install)

    Archivo de configuracion:

        renombramos el archivo config.sample por config.js, que se encuentra en la ruta
        app/config/

        Agregamos nuestras configuraciones a dicho archivo.

        Nota: Se puede utilizar la configuracion por defecto unicamente agregando las Twitter key y Twitter secret, que son necesarias para el inicio de session.

Iniciar aplicacion:

    Iniciamos la base datos mongo:

        mongod

    Iniciamos el Redis Server:

        redis-server

    Iniciamos la aplicacion

        node server.js
	

## La idea

Desarrollo de una aplicación que permita a los usuarios móviles sean estos ciudadanos de Corrientes o turistas informarse en cualquier lugar y en el momento oportuno sobre eventos próximos a realizarse.  
Los mismos estarán clasificados según diferentes categorías: Arte, Musical, Religioso, Académicos, Actividades de recreación, Deportivo y Vida nocturna.
La aplicación brindará a los usuarios móviles una herramienta que los ayudará a encontrar eventos de interés muy fácilmente y acceder a detalles del mismo. Permitirá registrar nuevos eventos, asociar una descripción entre otros detalles y visualizarlo mediante marcadores ubicados en un mapa geográfico, además podrá filtrar por categorías.
Con la implementación de la aplicación se pretende aportar a la gestión del Turismo de Corrientes mediante una herramienta que impulse y agregue valor a los servicios y promoción del Turismo que actualmente brindan a los ciudadanos y turistas.

## El desarrollo (v1.0)

Para esta primera versión la aplicación será soportada sobre Node Js y se ha pensado hacer el desarrollo en 3 fases:

### Fase 1 - Modelo de datos y gestión de usuarios

En esta primera fase vamos a definir cómo va ser nuestra base de datos para guardar usuarios, lugares y comentarios. Por último terminaremos por incoporar un sistema para gestionar registros y acceso de usuarios.


### Fase 2 - Mapa, introducción de lugares y sistema de comentarios

En esta fase vamos a hacer el formulario para agregar lugares, imágenes y un pequeño comentario, además de mostrarlo en el mapa en tiempo real.


### Fase 3 - Maquetación de la aplicación y adaptación a dispositivos móviles

Puliremos nuestra aplicación para mejorar la apariencia visual y además que se vea bien incluso desde un cepillo de dientes con pantalla y conexión a internet.